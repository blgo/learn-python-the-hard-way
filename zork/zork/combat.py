from random import randint
from zork.characters import Character

class Combat(object):

    def nothing_happens(self):
        print("Alien stares at you and you stare back, waiting for the next move...")
    
    def draw(self,player,alien):
        print("you receive a punch and you hit another one the Alien! Ouch!")
        alien.receive_hit()
        player.receive_hit()

    def player_counters(self,player,alien):

        print("Player dodges and counter attack! critical hit!")
        alien.receive_hit()
        alien.receive_hit()
        alien.receive_hit()
        alien.receive_hit()
        alien.receive_hit()

    def alien_counters(self,player,alien):

        print("Alien dodges and counter attack!")
        player.receive_hit()

    def alien_dodge_fails(self,player,alien):

        print("Alien tries to dodge attack but gets hit!")
        alien.receive_hit()
        alien.receive_hit()

    def player_dodge_fails(self,player,alien):
        print("Player tries to dodge attack but gets hit!")
        player.receive_hit()

    def start(self,player):

        self.player = player
        alien = Character()
        alien_choices = ["dodge left","dodge right","punch left","punch right"]
        print("You have encounter an Alien!")

        while player.healt_points > 0 and alien.healt_points > 0:

            print(f"Alien health {alien.healt_points}\n Your health: {player.healt_points}")
            player_choice = alien_choices[randint(0,3)].split(' ')
            alien_choice = alien_choices[randint(0,3)].split(' ')
                
            if alien_choice[0] == "punch" and player_choice[0] == "punch":
                self.draw(self.player,alien)

            elif alien_choice[0] == "dodge" and player_choice[0] == "punch":
                if alien_choice[1] != player_choice[1]:
                    self.alien_counters(self.player,alien)
                else:
                    self.alien_dodge_fails(self.player,alien)
            elif player_choice[0] == "dodge" and alien_choice[0] == "punch":
                if alien_choice[1] != player_choice[1]:
                    self.player_counters(self.player,alien)
                else:
                    self.player_dodge_fails(self.player,alien)
            else:
                self.nothing_happens()

        if player.healt_points > 0:
            print("Alien faint!")
            return False
        
        print("Alien killed you!")
        return True
