from sys import exit
from random import randint
#from textwrap import dedent
from zork.combat import Combat

class Scene(object):
    def __init__(self, name, description):
        self.name = name
        self.description = description
        self.paths = {}
    
    def go(self, direction):
        return self.paths.get(direction, None)

    def add_paths(self, paths):
        self.paths.update(paths)

    def enter(self,player):
        print(self.description)
        print("These are the paths available:")
        for path in self.paths:
            print(path,": ",self.paths[path])

        direction = input("> ")
        return self.go(direction)

class Map(object):

    def __init__(self, start_scene):
        self.start_scene = start_scene
        self.scenes = {}
    
    def next_scene(self, scene_name):
        return self.scenes.get(scene_name)

    def opening_scene(self):
        return self.next_scene(self.start_scene)

    def add_scene(self,scene):
        self.scenes.update(scene)
