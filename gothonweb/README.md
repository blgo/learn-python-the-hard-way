# Docker image

## Build docker image

`docker build . -t blgo/lpthw-docker:latest`

## Run image from Docker hub
### -v ~/Documents/PythonTheHardWay/projects/gothonweb is your Flask app

`docker run --rm -p 80:80 blgo/lpthw-docker:latest`
