from flask import Flask, session, redirect, url_for, escape, request
from flask import render_template
from gothonweb import planisphere


app = Flask(__name__)

@app.route("/")
def index():
    # this is used to "setup" the session with starting values
    session['count'] = 0
    session['room_name'] = planisphere.gothon.START
    return redirect(url_for("game"))


@app.route("/game", methods=['GET', 'POST'])
def game():
    room_name = session.get('room_name')
    if request.method == "GET":
        if room_name:
            room = planisphere.gothon.load_room(room_name)
            if "prelude" in room_name:
                return render_template("show_room_prelude.html", room=room)
            return render_template("show_room.html", room=room, count=session['count'])
        else:
            # why is there here? do you need it?'
            return render_template("you_died.html")
    else:
        action = request.form.get('action')
        if room_name and action:
            session['count'] += 1
            next_room = planisphere.gothon.load_room(room_name).go(action)
            if not next_room:
                session['room_name'] = room_name
            else:
                session['room_name'] = next_room
        return redirect(url_for("game"))


app.secret_key = '1Ai9Mk1fXnkN3VN1yTw445QZDokF4b'

if __name__ == "__main__":
    app.run(debug=True)
