#! /usr/local/lib/python

from sys import argv
import os
from ex46.rename_project import inplace_change, replace_rename


scriptname, NAME = argv

# We need the directories in the current path
rootDir = os.getcwd()
excluded_files = [".pyc", "setprojectnames.py"]

# NAME is the default project name for the project skeleton
# Rename all directories to set NAME to our project name
for dirpath, dirnames, filenames in os.walk(rootDir):
    for dirname in dirnames:
        replace_rename(NAME,dirpath,dirname)

# Rename all files to set NAME to our project name
for dirpath, dirnames, filenames in os.walk(rootDir):
    for filename in filenames:
        replace_rename(NAME,dirpath,filename)

# Walk thought directory once again to update filenames and
# substitute infile to set NAME to our project name
for dirpath, dirnames, filenames in os.walk(rootDir):
    for filename in filenames:
        inplace_change(NAME, dirpath, filename, excluded_files)
