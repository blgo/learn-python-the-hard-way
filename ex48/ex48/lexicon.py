def convert_number(s):
    try:
        return int(s)
    except ValueError:
        return None

def scan(input_string):
    directions = ["north", "south", "east", "west", "down", "up", "left", "right", "back"]
    verbs = ["go", "stop", "kill", "eat"]
    stops = ["in", "of", "from", "at", "it", "the"]
    nouns = ["door", "bear", "princess", "cabinet"]
    tuple_words = []
    
    input_words = input_string.split(' ')
    for input_word in input_words:
        if input_word in directions:
            this_touple = ("direction", input_word)
        elif input_word in verbs:
            this_touple = ("verb", input_word)
        elif input_word in stops:
            this_touple = ("stop", input_word)
        elif input_word in nouns:
            this_touple = ("noun", input_word)
        elif convert_number(input_word):
            this_touple = ("number", int(input_word))   
        else:
            this_touple = ("error", input_word)
        tuple_words.append(this_touple)
    return tuple_words
