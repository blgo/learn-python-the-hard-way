from nose.tools import *
from main import app,game,index
from io import BytesIO

app.config['TESTING'] = True
web = app.test_client()

def test_requests_to_app():
    rv = web.get('/NoGame', follow_redirects=True)
    assert_equal(rv.status_code, 404)

    rv = web.get('/', follow_redirects=True)
    assert_equal(rv.status_code, 200)
    assert_in(b"Corridor", rv.data)

    data = {'action':'tell a joke'}
    rv = web.post('/game', follow_redirects=True, data=data)
    assert_in(b"Gothon insults in the academy", rv.data)


    # This benhaviour will be modified
    # If the user enters a bad action, which does not match 
    # any of the paths, the engine might give you tips
    data = {'action':''}
    rv = web.post('/game', follow_redirects=True, data=data)
    assert_in(b"Gothon insults in the academy", rv.data)
    data = {'action':'telal as jokae'}
    rv = web.post('/game', follow_redirects=True, data=data)
    assert_in(b"Gothon insults in the academy", rv.data)

    # Reproduce behaviour of bomb lock fusing 
    # when wrong password is entered
    data = {'action':'0123'}
    rv = web.post('/game', follow_redirects=True, data=data)
    assert_in(b"Gothon insults in the academy", rv.data)


def test_app_context():
    # with app.test_context():
        # Test your app context code: current_app, g, url_for
    pass

def test_app_request_context():
    #with app.test_request_context():
        # Test your app request context code: request, session
    pass
    