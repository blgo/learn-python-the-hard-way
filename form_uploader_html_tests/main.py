import os
from flask import Flask, request, url_for, send_from_directory, render_template, redirect


app = Flask(__name__)

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'


@app.route('/hello/', methods=['POST', 'GET'])
@app.route('/', methods=['GET'])
def index():
    greeting = "Hello Form!"

    if request.method == "POST":
        name = request.form['name']
        greet = request.form['greet']
        greeting = f"{greet}, {name}"
        return render_template("index_laid_out.html", greeting=greeting)
    else:
        return render_template("hello_form_laid_out.html", greeting=greeting)

@app.route('/upload/', methods=['POST','GET'])
def upload_file():
    # Get the name of the uploaded file
    if request.method == "POST":
        file = request.files['file']
        if file:
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
            return redirect(url_for('uploaded_file', filename=file.filename))
        else:
            return render_template("upload_file.html")    
    else:
         return render_template("upload_file.html")

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=5000)
