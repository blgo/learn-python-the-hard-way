from nose.tools import *
from zork.rooms import *
from zork.characters import Character

def test_adding_scenes_to_map():
    a_map = Map("central_corridor")
    central_corridor = Scene("central_corridor","Desciption for central_corridor")
    central_corridor_scene = {"central_corridor" : central_corridor}
    a_map.add_scene(central_corridor_scene)
    current_scene = a_map.opening_scene()

    assert_equal(current_scene.name,"central_corridor")
    assert_equal(current_scene.description,"Desciption for central_corridor")
    return a_map

def test_adding_path_to_scene():
    a_map = test_adding_scenes_to_map()
    test_adding_scenes_to_map()

    current_scene = a_map.opening_scene()

    path={"here": "central_corridor"}
    current_scene.add_paths(path)

    # Name of the next scene to be retrieved from the dictionary stored on a_map
    next_scene_name = current_scene.go("here")
    # Get the object which correspond to the new destination
    next_scene= a_map.next_scene(next_scene_name)
    # If you go "here", you are in the same room!
    assert_equal(next_scene,current_scene)
    
    return a_map

#def test_enter_scene():
#    a_map = test_adding_path_to_scene()
#    current_scene = a_map.opening_scene()
#    player = Character()
#    next_scene_name = current_scene.enter(player)
#    next_scene= a_map.next_scene(next_scene_name)
#   assert_equal(next_scene,current_scene)