try: 
    from setptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'My Project',
    'author': 'Blas Gomez',
    'url': 'https://github.com/blgo/',
    'download_url': 'https://github.com/blgo/',
    'author_email': 'jblas1989@gmail.com',
    'version': '0.1',
    'install_requires': ['nose'],
    'packages': ['ex48'],
    'scripts': [],
    'name': 'projectname'
}

setup(**config)