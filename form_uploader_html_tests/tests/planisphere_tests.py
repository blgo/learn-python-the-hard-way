from nose.tools import *
from gothonweb.planisphere import *


def test_room():
    gold = Room("GoldRoom",
                """This room has fold in it you can grab. 
                There's a door to the North.""")
    assert_equal(gold.name, "GoldRoom")
    assert_equal(gold.paths, {})


def test_room_paths():
    center = Room("Center", "Test room in the Center.")
    north = Room("North", "Test room in the North")
    south = Room("South", "Test room in the South")

    center.add_paths({'north': north, 'south': south})
    assert_equal(center.go('north'), north)
    assert_equal(center.go('south'), south)


def test_map():
    start = Room("Start", "You can go west and down a hole.")
    west = Room("Trees", "Where are trees here, you can go east.")
    down = Room("Dungeon", "It's dark down here, you can go up.")

    start.add_paths({'west': west, 'down': down})
    west.add_paths({'east': start})
    down.add_paths({'up': start})

    assert_equal(start.go('west'), west)
    assert_equal(start.go('west').go('east'), start)
    assert_equal(start.go('down').go('up'), start)


def test_gothon_game_map():
    start_room = load_room(START)
    assert_equal(start_room.go('shoot!'), central_corridor_shoot_death)
    assert_equal(start_room.go('dodge!'), central_corridor_dodge_death)
    room = start_room.go('tell a joke')
    assert_equal(room, laser_weapon_armory)

    assert_equal(room.go('*'), laser_weapon_armory_death)
    room = room.go('0132')
    assert_equal(room, the_bridge)

    assert_equal(room.go('throw the bomb'), the_bridge_death)
    room = room.go('slowly place the bomb')
    assert_equal(room, escape_pod)

    assert_equal(room.go('*'), the_end_loser)
    assert_equal(room.go('2'), the_end_winner)