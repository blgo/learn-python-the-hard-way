import re
import os

def inplace_change(projectname, path, name, excluded_files):
    fullpath = os.path.join(path, name)
    if not any(string in fullpath for string in excluded_files):
        with open(fullpath) as f:
            print(fullpath)
            s = f.read()
            if "NAME" not in s:
                return 0
        with open(fullpath,'w') as f:
            s = s.replace("NAME",projectname)
            f.write(s)

def replace_rename(projectname, path, name):
    fullpath = os.path.join(path, name)
    print(fullpath)
    replaced_fullpath = re.sub("NAME",projectname,fullpath)
    os.rename(fullpath,replaced_fullpath)
