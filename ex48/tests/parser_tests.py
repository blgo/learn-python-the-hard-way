from nose.tools import *
from ex48.parser import *

def test_match():
    words = [('verb','go'),('direction','north'),('direction','south'),('direction','east'),('direction','west'),('verb','kill')]
    word = match(words,'verb')
    assert_equal(word[1],'go')
    word = match(words,'direction')
    assert_equal(word[1],'north')
    word = match(words,'direction')
    assert_equal(word[1],'south')
    word = match(words,'direction')
    assert_equal(word[1],'east')
    word = match(words,'direction')
    assert_equal(word[1],'west')
    word = match(words,'verb')
    assert_equal(word[1],'kill')

def test_sentence():
    words = [('stop','then'),('verb','go'),('stop','then'),('direction','north')]
    sentence = parse_sentence(words)
    assert_equal(sentence.subj,'player')
    assert_equal(sentence.verb,'go')
    assert_equal(sentence.obj,'north')

    words = [('stop','then'),('noun','princess'),('verb','eat'),('noun','bear')]
    sentence = parse_sentence(words)
    assert_equal(sentence.subj,'princess')
    assert_equal(sentence.verb,'eat')
    assert_equal(sentence.obj,'bear')

    words = [('stop','then'),('verb','go'),('noun','Jonny'),('stop','then'),('verb','go'),('verb','go')]
    sentence = parse_sentence(words)
    assert_equal(sentence.subj,'player')
    assert_equal(sentence.verb,'go')
    assert_equal(sentence.obj,'Jonny')

def test_failssentence():
    words = [('direction','noth')]
    assert_raises(ParserError,parse_sentence,words)