from zork.rooms import Map
from zork.combat import Character

class Engine(object):

    def __init__(self, scene_map):
        self.scene_map = scene_map
        self.player = Character()
    
    def play(self):
        current_scene = self.scene_map.opening_scene()
        final_scene = self.scene_map.next_scene("finish")

        while current_scene != final_scene:
            current_scene_name = current_scene.enter(self.player)
            current_scene = self.scene_map.next_scene(current_scene_name)

        current_scene.enter()

